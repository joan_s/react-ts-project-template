import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import EslintWebpackPlugin from 'eslint-webpack-plugin';

type WebpackEnv = Record<string, boolean | string> & {
    WEBPACK_SERVE?: boolean;
    WEBPACK_BUILD?: boolean;
    WEBPACK_WATCH?: boolean;
};

const config = (env: WebpackEnv): webpack.Configuration => {
    return {
        mode: env.WEBPACK_BUILD ? 'production' : 'development',
        output: {
            filename: '[name].[contenthash:6].js',
            chunkFilename: '[name].[contenthash:6].js',
            // Path must be sets for clean-webpack-plugin to work, even if we use default.
            publicPath: '/',
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.jsx'],
            fallback: { crypto: false },
        },
        module: {
            rules: [
                {
                    oneOf: [
                        // Process source JavaScript|TypeScript files with Babel.
                        {
                            test: /\.(js|mjs|jsx|ts|tsx)$/,
                            include: path.resolve(__dirname, 'src'),
                            use: {
                                loader: require.resolve('babel-loader'),
                                options: {
                                    cacheDirectory: true,
                                    cacheCompression: false,
                                },
                            },
                        },
                        // Process sass|scss files with Sass.
                        {
                            test: /\.s[ac]ss$/,
                            use: [
                                env.WEBPACK_BUILD ? MiniCssExtractPlugin.loader : 'style-loader',
                                'css-loader',
                                'sass-loader',
                            ],
                        },
                        {
                            test: /\.css$/,
                            use: [env.WEBPACK_BUILD ? MiniCssExtractPlugin.loader : 'style-loader', 'css-loader'],
                        },
                    ],
                },
            ],
        },
        devtool: env.WEBPACK_BUILD ? 'source-map' : 'cheap-module-source-map',
        devServer: {
            static: {
                directory: path.join(__dirname, 'public'),
            },
            historyApiFallback: true,
            open: false,
        },
        plugins: [
            new CleanWebpackPlugin(),
            new EslintWebpackPlugin({
                extensions: ['js', 'jsx', 'ts', 'tsx'],
                files: ['src'],
                formatter: 'codeframe',
                threads: true,
            }),
            new ForkTsCheckerWebpackPlugin({
                typescript: {
                    diagnosticOptions: {
                        semantic: true,
                        syntactic: true,
                    },
                    mode: 'write-references',
                },
                formatter: {
                    type: 'codeframe',
                    options: {
                        highlightCode: true,
                    },
                },
            }),
            new HtmlWebpackPlugin({
                inject: true,
                template: path.resolve(__dirname, 'public/index.html'),
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true,
            }),
            new MiniCssExtractPlugin({
                filename: '[name].[contenthash:6].css',
                chunkFilename: '[name].[contenthash:6].css',
            }),
        ],
    };
};

export default config;
