import React from 'react';
import './App.scss';

const App = (): JSX.Element => {
    return (
        <div className="App">
            <header className="App-header">
                <img src="/logo.svg" className="App-logo" alt="logo" />
                <p>
                    Edit <code>src/app.tsx</code> and save to reload.
                </p>
                <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
                    Learn React
                </a>
            </header>
        </div>
    );
};

export default App;
